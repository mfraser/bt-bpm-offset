import pyjapc

myMonitor = pyjapc.PyJapc("PSB.USER.ALL", noSet = True)

bpm_list = ['BT3.BPM00','BT3.BPM10','BT3.BPM20','BT.BPM30','BT.BPM40']

for i in bpm_list:
    print(i)
    myKnob = myMonitor.getParam('rmi://inca_psb/'+ i + '/Offsets',timingSelectorOverride="")
    print(myKnob)
